﻿// HellpWorld.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
//  Вызываем функцию std::cout
void print()
{
    /* Потомучто хотим использовать стандартную библиотеку для печати в консоль и
    * не использовать какие либо другие библиотеки.
    */
    std::cout << "Hello Skillbox!\n";
}

int sum(int a, int b)
{
    return (a + b);
}

int printint(int toprint)
{
    std::cout << toprint << "\n" ;
    return toprint;

}

/*
* Присваеваем значение переменным и печатаем Hello World!
*/

int main()
{ 
    std::cout << sum(100,500)  << "\n";
    return 0;

}

